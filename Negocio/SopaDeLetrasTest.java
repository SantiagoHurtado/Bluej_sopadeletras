package Negocio;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaDeLetrasTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetrasTest
{
    /**
     * Default constructor for test class SopaDeLetrasTest
     */
    public SopaDeLetrasTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    { 
        
    }
    
    @Test
    public void crearMatriz() throws Exception
    {
    String palabras="marc,pe,a";
    SopaDeLetras s=new SopaDeLetras(palabras);
    
    char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
    
    char esperado[][]=s.getSopas();
    // :) --> ideal == esperado
    
    boolean pasoPrueba=sonIguales(ideal, esperado);
    assertEquals(true,pasoPrueba);
    
    }
    
    
    @Test
    public void crearMatriz_conError() throws Exception
    {
    String palabras="marc,pe,a";
    SopaDeLetras s=new SopaDeLetras();
    
    char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
    char esperado[][]=s.getSopas();
    // :) --> ideal == esperado
    
    boolean pasoPrueba=sonIguales(ideal, esperado);
    assertEquals(false,pasoPrueba);
    
    }
    
    
    
    
    private boolean sonIguales(char m1[][], char m2[][])
    {
        
        if(m1==null || m2==null || m1.length !=m2.length)
            return false; 
            
      int cantFilas=m1.length; 
      
      for(int i=0;i<cantFilas;i++)
      {
          if( !sonIgualesVector(m1[i],m2[i]))
            return false;
      }
      
        
    return true;
    }
    
    
    private boolean sonIgualesVector(char v1[], char v2[])
    {
        if(v1.length != v2.length)
                return false;
         
        for(int j=0;j<v1.length;j++)
      {
            if(v1[j]!=v2[j])
                return false;
      }
      return true;
    }
}
